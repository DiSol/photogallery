var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var jwt = require('express-jwt');
var azure = require('azure-storage');
var multer = require('multer');
var http = require('http');

var router = express.Router();

require('../model/User');

var User = mongoose.model('User');

var auth = jwt({secret: 'SECRET', userProperty: 'payload'});

var blobService = azure.createBlobService('exphotogallery', 'AZ2c2zOBThUk3XCmQaO7op5GFyZlv2A6Ukkte50mzGxa+/6ctwW67YX+XNGxOXaNG9p/4LgiO3ulWH4OmAyJtg==');

var manipulateUser = function(username, req, res, callback) {
	User.findOne({ username: username }, function (err, user) {
		if (err) {
			return res.send(err);
		}
		
		if (user !== null) {
			callback(user, function(afterSaveCallback) {
				user.save(function(err) {
					if (err) {
						res.send(err);
					}
					
					if (afterSaveCallback)
						afterSaveCallback();
				});
			});
		}
	});
};

router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

router.post('/register', function(req, res, next) {
	if (!req.body.username || !req.body.password) {
		return res.status(400).json({message: 'Please fill out all fields'});
	}

	var user = new User();
	user.username = req.body.username;
	user.setPassword(req.body.password)

	user.save(function (err) {
		if (err) {
			return next(err);
		}

		return res.json({token: user.generateJWT()})
	});
});

router.post('/login', function(req, res, next) {
	if (!req.body.username || !req.body.password) {
		return res.status(400).json({message: 'Please fill out all fields'});
	}

	passport.authenticate('local', function(err, user, info) {
		if (err) {
			return next(err);
		}

		if (user) {
			return res.json({token: user.generateJWT()});
		}
		else {
			return res.status(401).json(info);
		}
	})(req, res, next);
});

var sendAlbums = function(req, res) {
	var containers = [];
	
	var aggregate = function(error, result, callback) {
		if (!error) {
			containers = containers.concat(result.entries);
			if (result.continuationToken !== null) {
	            blobService.listContainersSegmented(result.continuationToken, aggregate);
	        } else {
	        	callback(null, containers);
	        }
		}
		else {
			callback(error);
		}
	};

	blobService.listContainersSegmented(null, function(error, result, response) {			
		aggregate(error, result, function(error, result) {
			if (!error) {
				var result = result.map(function(item) {
					return {
						name: item.name
					}
				});
				res.json(result);
			}
			else {
				res.json(error);
			}
		});
	});
}

router.route('/albums')
	.get(auth, function(req, res, next) {
		sendAlbums(req, res);
	});

var cleanUserPermissionForAlbum = function(user, permission, album) {
	while ((idx = user[permission].indexOf(album)) !== -1) {
		user[permission].splice(idx, 1);
	}
}

router.route('/albums/:album')
	.put(auth, function(req, res, next) {
		blobService.createContainerIfNotExists(req.params.album, {publicAccessLevel : 'blob'}, function(error, result, response) {
			if (error) {
				res.json(error);
				return;
			}
			
			manipulateUser(req.payload.username, req, res, function(user, save) {
				user.owner_albums.push(req.params.album);
				user.read_albums.push(req.params.album);
				user.write_albums.push(req.params.album);
				
				save(function() {
					res.json({token: user.generateJWT()});
				});
			});
		});
	})
	.delete(auth, function(req, res, next) {
		blobService.deleteContainerIfExists(req.params.album, function(error, result, response) {
			if (error) {
				res.json(error);
				return;
			}
			
			User.find().sort({'username': 'asc'}).exec(function(err, users) {
				if (err) {
					res.send(err);
				}
				
				users.forEach(function(user) { 
					manipulateUser(user.username, req, res, function(user, save) {
						cleanUserPermissionForAlbum(user, 'owner_albums', req.params.album);
						cleanUserPermissionForAlbum(user, 'read_albums', req.params.album);
						cleanUserPermissionForAlbum(user, 'write_albums', req.params.album);

						save();
					});	
				});
				
				sendAlbums(req, res);
			});
		});
	});

var sendPhotos = function(album, res) {
	blobService.listBlobsSegmented(album, null, /*{maxResults: 12}, */function(error, result, response) {
		if (!error) {
			var result = result.entries.map(function(item) {
				return {
					name: item.name,
					url: 'https://exphotogallery.blob.core.windows.net/' + album + '/' + item.name
				};
			});
			res.json(result);
		}
		else {
			res.json(error);
		}
	});
}

var updateElasticSearch = function(album, photo, size) {
	var options = {
		host: 'exelastic.cloudapp.net',
		port: 9200,
		path: '/photogallery/photo/' + photo,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		}
	};
	
	var body = JSON.stringify({
		name: photo,
		album: album,
		size: size,
		timestamp: new Date().getTime()
	});
	
	console.log('sending to ES: ' + body);
	
	var post_req = http.request(options, function(res) {
		
	});
	
	post_req.on('error', function(e) {
		console.log('problem with request: ' + e.message);
	});
	
	post_req.write(body);
	post_req.end();
}

router.route('/albums/:album/photos')
	.post([auth, multer({
		putSingleFilesInArray: true,
		dest: './uploads/',
		rename: function(fieldname, filename) {
			return filename;
		},
		changeDest: function(dest, req, res) {
			return dest;
		},
		onError: function(error, next) {
			next();
		},
		onFileUploadStart: function(file, req, res) {
			console.log('upload started: ' + file.name);
			file.blobWriteStream = blobService.createWriteStreamToBlockBlob(req.params.album, file.name);
			return true;
		},
		onFileUploadData: function(file, data, req, res) {
			file.blobWriteStream.write(data);
			console.log('uploading ' + file.name + ': ' + data.length + ' arrived');
		},
		onFileUploadComplete: function(file, req, res) {
			console.log('upload completed: ' + file.name);
			file.blobWriteStream.end();
			
			updateElasticSearch(req.params.album, file.name, file.size);
		}}), function(req, res, next) {
			res.end();
	}])
	.get(function(req, res, next) {
		sendPhotos(req.params.album, res);
	});

router.route('/albums/:album/photos/:photo')
	.delete(auth, function(req, res, next) {
		blobService.deleteBlobIfExists(req.params.album, req.params.photo, function(error, result, response) {
			if (error) {
				res.json(error);
				return;
			}
			
			sendPhotos(req.params.album, res);
		});
	});

var possiblePermissions = {
	can_read: 'read_albums',
	can_write: 'write_albums'
};

var sendPermissions = function(req, res) {
	User.find().sort({'username': 'asc'}).exec(function(err, users) {
		if (err) {
			res.send(err);
		}
		
		var per_res = [];
		
		users.forEach(function(user) {
			per_res.push({
				username: user.username, 
				is_owner: user.owner_albums.indexOf(req.params.album) == -1 ? false : true, 
				can_read: user.read_albums.indexOf(req.params.album) == -1 ? false : true,
				can_write: user.write_albums.indexOf(req.params.album) == -1 ? false : true
			});
		});
		
		res.json(per_res);
	});
}

router.get('/albums/:album/permissions', auth, function(req, res, next) {
	sendPermissions(req, res);
});

router.route('/albums/:album/permissions/:user/:permission')
	.put(auth, function(req, res, next) {
		manipulateUser(req.params.user, req, res, function(user, save) {
			if (req.params.permission in possiblePermissions) {
				user[possiblePermissions[req.params.permission]].push(req.params.album);
				
				save(function() {
					sendPermissions(req, res);	
				});
			}
			else {
				next();
			}
		});
	})
	.delete(auth, function(req, res, next) {
		manipulateUser(req.params.user, req, res, function(user, save) {
			if (req.params.permission in possiblePermissions) {
				var per = possiblePermissions[req.params.permission];
				user[per].splice([user[per].indexOf(req.params.album)], 1);
				
				save(function() {
					sendPermissions(req, res);
				});
			}
			else {
				next();
			}
		});
	});

module.exports = router;
