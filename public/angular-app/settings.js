'use strict'

angular.module('photoGallery')
.factory('settings', ['$http', 'auth', function($http, auth) {
	var service = {
		permissions: []
	}
	
	service.getPermissions = function(album) {
		return $http.get('/albums/' + album.name + '/permissions', {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data, service.permissions);
		});
	};
	
	service.setPermission = function(album, user, permission) {
		return $http.put('/albums/' + album.name + '/permissions/' + user.username + '/' + permission, {}, {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data, service.permissions);
		});
	};
	
	service.deletePermission = function(album, user, permission) {
		return $http.delete('/albums/' + album.name + '/permissions/' + user.username + '/' + permission, {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data, service.permissions);
		});
	};
	
	return service;
}]);