'use strict'

angular.module('photoGallery')
.factory('albums', ['$http', '$state', 'auth', 'app_state', function($http, $state, auth, app_state) {
	var service = {
		albums: [],
		photos: []
	}
	
	service.getAll = function() {
		return $http.get('/albums', {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data.filter(function(album) {
				return auth.canReadAlbum(album);
			}), service.albums);
			if (service.albums.length != 0) {
				app_state.currentAlbum = service.albums[0];
				service.getAlbumPhotos(app_state.currentAlbum);
			}
		});
	};
	
	service.create = function(album) {
		return $http.put('/albums/' + album.name, {}, {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			auth.saveToken(data.token);
			service.getAll();
		});
	};

	service.delete = function(album) {
		return $http.delete('/albums/' + album.name, {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data, service.albums);
			if (service.albums.length != 0) {
				app_state.currentAlbum = service.albums[0];
				service.getAlbumPhotos(app_state.currentAlbum);
				$state.go('albums');
			}
		});
	};
	
	service.getAlbumPhotos = function(album) {
		return $http.get('/albums/' + album.name + '/photos', {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data, service.photos);
		});
	};
	
	service.deletePhoto = function(album, photo) {
		return $http.delete('/albums/' + album.name + '/photos/' + photo, {
			headers: {Authorization: 'Bearer ' + auth.getToken()}
		}).success(function(data) {
			angular.copy(data, service.photos);
		});
	};
	
	return service;
}]);