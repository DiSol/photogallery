var app = angular.module('photoGallery', ['ui.router', 'ngFileUpload']);

app.factory('app_state', [function() {
	var state = {
		currentAlbum: null
	};
	
	return state;
}]);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('home', {
		url: '/',
		templateUrl: 'angular-app/components/home.html'
	})
	.state('albums', {
		url: '/albums',
		templateUrl: 'angular-app/components/albums.html',
		controller: 'AlbumsCtrl',
		resolve: ['albums', function(albums) {
			return albums.getAll();
		}]
	})
	.state('settings', {
		url: '/settings',
		templateUrl: 'angular-app/components/settings.html',
		controller: 'SettingsCtrl',
		resolve: ['settings', 'app_state', function(settings, app_state) {
			return settings.getPermissions(app_state.currentAlbum);
		}]
	})
	.state('login', {
		url: '/login',
		templateUrl: 'angular-app/components/login.html',
		controller: 'AuthCtrl',
		onEnter: ['$state', 'auth', function($state, auth) {
			if (auth.isLoggedIn()) {
				$state.go('albums');
			}
		}]
	})
	.state('register', {
		url: '/register',
		templateUrl: 'angular-app/components/register.html',
		controller: 'AuthCtrl',
		onEnter: ['$state', 'auth', function($state, auth) {
			if (auth.isLoggedIn()) {
				$state.go('albums');
			}
		}]
	});

//	$urlRouterProvider.otherwise('albums');
}]);