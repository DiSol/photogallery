'use strict'

angular.module('photoGallery')
.factory('auth', ['$http', '$window', function($http, $window) {
	var auth = {};

	auth.saveToken = function(token) {
		$window.localStorage['photoGallery-token'] = token;
	};

	auth.getToken = function() {
		return $window.localStorage['photoGallery-token'];
	}

	auth.isLoggedIn = function() {
		var token = auth.getToken();

		if (token) {
			var payload = JSON.parse($window.atob(token.split('.')[1]));
			return payload.exp > Date.now() / 1000;
		} else {
			return false;
		}
	};

	auth.currentUser = function() {
		if (auth.isLoggedIn()) {
			var token = auth.getToken();
			var payload = JSON.parse($window.atob(token.split('.')[1]));
			return payload;
		}
	};

	auth.canReadAlbum = function(album) {
		return auth.currentUser().read_albums.indexOf(album.name) !== -1;
	}
	
	auth.canWriteAlbum = function(album) {
		return auth.currentUser().write_albums.indexOf(album.name) !== -1;
	}
	
	auth.canDeleteAlbum = function(album) {
		return auth.currentUser().owner_albums.indexOf(album.name) !== -1;
	}

	auth.register = function(user) {
		return $http.post('/register', user).success(function(data) {
			auth.saveToken(data.token);
		});
	};

	auth.logIn = function(user) {
		return $http.post('/login', user).success(function(data) {
			auth.saveToken(data.token);
		});
	};

	auth.logOut = function() {
		$window.localStorage.removeItem('photoGallery-token');
	};

	return auth;
}]);