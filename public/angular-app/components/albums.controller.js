'use strict'

angular.module('photoGallery')
.controller('AlbumsCtrl', ['$scope', 'albums', 'auth', 'Upload', 'app_state', function($scope, albums, auth, Upload, app_state) {
	$scope.albums = albums.albums;
	$scope.photos = albums.photos;
	
	$scope.isCurrent = function(album) {
		return $scope.currentAlbum.name == album.name;
	}
	
	$scope.setCurrent = function(album) {
		$scope.currentAlbum = album;
		app_state.currentAlbum = $scope.currentAlbum;
		albums.getAlbumPhotos(album);
	}
	
	$scope.canWrite = function() {
		return auth.canWriteAlbum($scope.currentAlbum);
	}
	
	$scope.deletePhoto = function(photo) {
		albums.deletePhoto($scope.currentAlbum, photo);
	}
	
	if ($scope.albums.length != 0) {
		if (app_state.currentAlbum)
			$scope.currentAlbum = app_state.currentAlbum;
		else
			$scope.setCurrent($scope.albums[0]);
	}
	
	$scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

	$scope.upload = function (files) {
		if (files && files.length) {
			Upload.upload({
				headers: {Authorization: 'Bearer ' + auth.getToken()},
				url: 'albums/' + $scope.currentAlbum.name + '/photos',
				fields: {'username': $scope.username},
				file: files
			}).progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
			}).success(function (data, status, headers, config) {
				albums.getAlbumPhotos($scope.currentAlbum);
				$scope.photos = albums.photos;
			});
		}
	};
}]);