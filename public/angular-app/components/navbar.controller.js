'use strict';

angular.module('photoGallery')
.controller('NavbarCtrl', ['$scope', '$state', 'auth', 'albums', function($scope, $state, auth, albums) {
	$scope.isLoggedIn = auth.isLoggedIn;
	$scope.currentUser = auth.currentUser;
	
	$scope.logOut = function() {
		auth.logOut();
		$state.go('home');
	}
	
	$scope.create = function() {
		albums.create($scope.album).error(function(error) {
			$scope.error = error;
		});
	};
}]);