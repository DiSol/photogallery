'use strict';

angular.module('photoGallery')
.controller('SettingsCtrl', ['$scope', 'settings', 'app_state', 'albums', 'auth', function($scope, settings, app_state, albums, auth) {
	$scope.users_permissions = settings.permissions;
	$scope.album = app_state.currentAlbum;
	
	$scope.changeReadPermission = function(user) {
		$scope.changePermission(user, 'can_read');
	}
	
	$scope.changeWritePermission = function(user) {
		$scope.changePermission(user, 'can_write');
	}
	
	$scope.changePermission = function(user, permission) {
		if (user[permission])
			settings.setPermission($scope.album, user, permission);
		else
			settings.deletePermission($scope.album, user, permission);
	}
	
	$scope.deleteAlbum = function() {
		albums.delete($scope.album);
	}
	
	$scope.canWrite = function() {
		return auth.canWriteAlbum($scope.album);
	}
}]);